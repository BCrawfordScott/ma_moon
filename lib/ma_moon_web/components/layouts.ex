defmodule MaMoonWeb.Layouts do
  use MaMoonWeb, :html

  embed_templates "layouts/*"
end
