defmodule MaMoonWeb.PageHTML do
  use MaMoonWeb, :html

  embed_templates "page_html/*"
end
