defmodule MaMoon.Repo do
  use Ecto.Repo,
    otp_app: :ma_moon,
    adapter: Ecto.Adapters.Postgres
end
