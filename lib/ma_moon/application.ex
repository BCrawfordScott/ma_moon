defmodule MaMoon.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      MaMoonWeb.Telemetry,
      MaMoon.Repo,
      {DNSCluster, query: Application.get_env(:ma_moon, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: MaMoon.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: MaMoon.Finch},
      # Start a worker by calling: MaMoon.Worker.start_link(arg)
      # {MaMoon.Worker, arg},
      # Start to serve requests, typically the last entry
      MaMoonWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MaMoon.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    MaMoonWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
